# MatRiCT-Au

The folders `n10m1`, `n10m2`, `n50m1`, `n50m2`, `n100m1`, and `n100m2` are the implementations for (N, M) = (10, 1), (10, 2), (50, 1), (50, 2), (100, 1), and (100, 2), respectively. 

In order to compile the source code, you need the [XKCP](https://github.com/XKCP/XKCP) i.e. Keccak library.

1. Run `make` to compile the source code.

2. Run `./ringct` to start the benchmark. The first 5 values are the runtime (in CPU cycles) of SamMat, Spend, Verify, TdRowGen, and Audit, respectively. The 6th value is the correctness ("1" means the transaction can pass the verification, and "0" otherwise). The 7th and 8th values are the account l from the input and from the Audit output, respectively. The (9th, 10th) and (11th, 12th) values are the output amounts `amt_out` from the input and from the Audit output, respectively.

The transaction generation time in the paper is the sum of SamMat and Spend. 
