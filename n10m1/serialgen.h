/* ****************************** *
 * Implemented by Raymond K. ZHAO *
 *                                *
 * SerialGen                      *
 * ****************************** */

#ifndef _SERIALGEN_H
#define _SERIALGEN_H

#include "poly_q.h"

void serialgen(POLY_Q *s, const POLY_Q *sk, const POLY_Q *h);

#endif
