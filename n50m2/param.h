/* ****************************** *
 * Implemented by Raymond K. ZHAO *
 *                                *
 * constants                      *
 * ****************************** */

#ifndef _PARAM_H
#define _PARAM_H

#include "poly_param.h"

#define CRYPTO_BYTES 32

#define B 1LL
#define BETA 50LL
#define R 64LL
#define N 18LL
#define M 38LL
#define N_HAT 35LL
#define M_HAT 69LL
#define W 56LL
#define P 8LL
#define BE 1LL

#define M_SPENT 2LL
#define S 2LL
#define N_SPENT BETA
#define N_SPENT_LOG 6

#define V_HAT (2 * (BETA + 2 * (R - 1) + S * R))
#define T (S + 3)

#define V_1 (BETA + S * R + 2 * (R - 1))
#define V_2 (BETA + S * R + 2 * (R - 1))

#define TD_T 5
#define TD_D 60
#define TD_L 4

#define E_BOUND 3300831713156LL

#define BBIG 6537216LL
#define BBIG_K_PRIME 13074432LL
#define BA 10240LL
#define BR 98304LL
#define BAP (BA - P)
#define BRP (BR - P)

#define F00_NORM (BA * BA * D * (BETA - 1))

/* #define TG "24509458294820727699800064" */
#define TG_COEFF_0 0
#define TG_COEFF_1 1702887424
#define TG_COEFF_2 1328660
#define TG_COEFF_3 0

#define BBIG_HAT 79134720LL
#define ZB_NORM (BBIG_HAT - B * P * W)
#define ZC_NORM (BBIG - B * P * W)
#define Z0_NORM (BBIG - B * P * W)
#define ZM_NORM (BBIG_K_PRIME - (M_SPENT + S + 1) * B * P * W)

#define REJ_UNIF_Q_BYTES 4
#define REJ_UNIF_Q_G0_LEN 43836
#define REJ_UNIF_Q_GR_LEN 117594
#define REJ_UNIF_Q_H_LEN 2458
#define REJ_UNIF_Q_BOUND 4294443026

#define REJ_UNIF_QBIG_BYTES 7
#define REJ_UNIF_QBIG_LEN 1516595
#define REJ_UNIF_QBIG_BOUND 72056460838172417LL

#define REJ_UNIF_QBIG1_BYTES 4
#define REJ_UNIF_QBIG1_LEN 2193
#define REJ_UNIF_QBIG1_BOUND 4294901792LL
#define REJ_UNIF_QBIG2_BYTES 4
#define REJ_UNIF_QBIG2_LEN 2187
#define REJ_UNIF_QBIG2_BOUND 4294965256LL

#define SAMPLE_B_MD_LEN 2508
#define SAMPLE_B_MHATD_LEN 4515
#define SAMPLE_B_MAX_LEN 4515
#define SAMPLE_B_BOUND 255

#define SAMPLE_BE_MHATD_LEN 4515
#define SAMPLE_BE_V1D_LEN 19676
#define SAMPLE_BE_V2D_LEN 19676
#define SAMPLE_BE_MAX_LEN 19676
#define SAMPLE_BE_BOUND 255

#define SAMPLE_BBIG_MD_LEN 2485
#define SAMPLE_BBIG_MAX_LEN 4528
#define SAMPLE_BBIG_BYTE 4
#define SAMPLE_BBIG_BOUND 4288414024

#define SAMPLE_BBIG_K_PRIME_MD_LEN 2485
#define SAMPLE_BBIG_K_PRIME_BOUND 4288413860

#define SAMPLE_BBIG_HAT_MHATD_LEN 4528
#define SAMPLE_BBIG_HAT_BOUND 4273274907

#define SAMPLE_BSTAR_BYTE 3
#define SAMPLE_BSTAR_MAX_LEN 90

#define SAMPLE_BA_LEN 79
#define SAMPLE_BA_BOUND 16773939

#define SAMPLE_BR_LEN 90
#define SAMPLE_BR_BOUND 16711765

#define SAMPLE_BAP_LEN 84
#define SAMPLE_BAP_BOUND 16760835

#define SAMPLE_BETA_LEN 13
#define SAMPLE_BETA_BYTE 2
#define SAMPLE_BETA_BOUND 65513

#define BARRETT_FACTOR_B 85
#define BARRETT_SHIFT_B 8

#define BARRETT_FACTOR_BE 85
#define BARRETT_SHIFT_BE 8

#define BARRETT_FACTOR_BBIG 328
#define BARRETT_FACTOR_BBIG_K_PRIME 164
#define BARRETT_FACTOR_BBIG_HAT 27
#define BARRETT_SHIFT_BBIG 32

#define BARRETT_SHIFT_BSTAR 24
#define BARRETT_FACTOR_BA 819
#define BARRETT_FACTOR_BR 85
#define BARRETT_FACTOR_BAP 819

#define BARRETT_FACTOR_BETA 1337
#define BARRETT_SHIFT_BETA 16

#define HASH_INPUT_BYTES 4
#if M_SPENT == 1
#define HASH_INPUT_LEN                                                       \
	(2 * N_HAT * D + 2 * N_HAT * D + N * D + N * D + N * D + N * D + D + \
	 N * D + N * D + D + N_SPENT * (N * D + N * D) + N * D + N * D +     \
	 N * D + N * D)
#else
#define HASH_INPUT_LEN                                                       \
	(2 * N_HAT * D + 2 * N_HAT * D + N * D + N * D + N * D + N * D + D + \
	 N * D + N * D + D + N_SPENT * (N * D + N * D) + N * D + N * D +     \
	 N * D + N * D + N * D + D + D + N_SPENT * (N * D + N * D))
#endif
#define HASH_OUTPUT_LEN 86
#define HASH_COEFF_LEN 28

#endif
